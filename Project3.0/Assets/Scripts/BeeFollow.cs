﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeFollow : MonoBehaviour
{
    [SerializeField] Transform Player; 
    private void FixedUpdate()
    {
        transform.LookAt(Player); 
    }
}
