﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; 
using UnityEngine.UI; 
using UnityEngine.Audio; 

public class PlayerMovement : MonoBehaviour
{
    [Header("Player Controller")]
    [SerializeField] Camera playerCamera; 
    [SerializeField] float playerSpeed = 2.0f;
    [SerializeField] float jumpHeight = 1.0f;
    [SerializeField] float gravityValue = -9.81f;
    public static PlayerController playerController; 
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    private InputManager inputManager; 
    private Transform cameraTransform; 
    bool haveQueen = false; 

    [Header("LayerMasks")]
    //suck and shoot gun mechanics
    float rayLength = 1f;
    public LayerMask leMask; 
    bool firstnote = true; 
    bool haveKey = false; 
    public LayerMask keyMask; 
    bool firstBees = true;
    bool havefirstkey = false; 
    bool firstDoor = false; 
    [SerializeField] LayerMask SecondFloor; //A5      //DOOR
    [SerializeField] LayerMask ThirdFloor; //A6       //DOOR
    [SerializeField] LayerMask FourthFloor; //A7      //DOOR
    [SerializeField] LayerMask FifthFloor; //A8       //DOOR
    [SerializeField] LayerMask Roofmask;  //A9        //DOOR
    [SerializeField] LayerMask queenMask; //A10       //BEES
    [SerializeField] LayerMask EndGameMask; //A11     //BEES
    [SerializeField] LayerMask doormask; //A12        //DOOR
    [SerializeField] LayerMask FindNemoKey; //A13     //KEY
    [SerializeField] LayerMask CakeBees; //A15        //BEES
    [SerializeField] LayerMask SmallSteps; //A4.2
    [SerializeField] LayerMask Search1; 
    [SerializeField] LayerMask Search2; 
    [SerializeField] LayerMask Search3; 
    [SerializeField] LayerMask Search4;
    [SerializeField] LayerMask Search5;
    [SerializeField] LayerMask Search6;
    [SerializeField] LayerMask Search7;
    [SerializeField] LayerMask Search8;     

    [Header("Achievements")]
    [SerializeField] Text achievementNo; 
    [SerializeField] GameObject stepsTrigger;
    float Achievements; 
    bool haveFirstStep = false; 
    bool haveCake = false; 
    bool savedTheBees = false; 
    bool FoundNemo = false;
    [SerializeField] ParticleSystem Confetti; 
    [SerializeField] GameObject A_1; //catch em all
    [SerializeField] GameObject A_2; //gain access
    [SerializeField] GameObject A_3; //uncovering secrets
    [SerializeField] GameObject A_4; //onesmallstep
    [SerializeField] GameObject A_5; //excel at exploring
    [SerializeField] GameObject A_6; //lunch time
    [SerializeField] GameObject A_7; //love is an open door
    [SerializeField] GameObject A_8; //meet me halfway
    [SerializeField] GameObject A_9; //do a flip
    [SerializeField] GameObject A_10; //one crown to rule them all
    [SerializeField] GameObject A_11; //save the bees
    [SerializeField] GameObject A_12; //open a door
    [SerializeField] GameObject A_13; //finding nemo
    [SerializeField] GameObject A_15; //the cake is a lie

    [Header("PauseMenuList")]
    [SerializeField] Text Catchem; 
    [SerializeField] Text GainAccess; 
    [SerializeField] Text OneSmallStep; 
    [SerializeField] Text ExcelAtExploring; 
    [SerializeField] Text LunchTime;
    [SerializeField] Text LoveIsAnOpenDoor; 
    [SerializeField] Text MeetMeHalfway; 
    [SerializeField] Text DoAFlip; 
    [SerializeField] Text OneCrown;
    [SerializeField] Text SaveTheBees; 
    [SerializeField] Text OpenADoor; 
    [SerializeField] Text FindingNemo;
    [SerializeField] Text TheCakeIsALie; 
    [SerializeField] GameObject Tick1; 
    [SerializeField] GameObject Tick2; 
    [SerializeField] GameObject Tick4; 
    [SerializeField] GameObject Tick5; 
    [SerializeField] GameObject Tick6; 
    [SerializeField] GameObject Tick7; 
    [SerializeField] GameObject Tick8; 
    [SerializeField] GameObject Tick9; 
    [SerializeField] GameObject Tick10; 
    [SerializeField] GameObject Tick11; 
    [SerializeField] GameObject Tick12; 
    [SerializeField] GameObject Tick13; 
    [SerializeField] GameObject Tick14; 


    [Header("Audio")]
    [SerializeField] GameObject Sounds; 
    [SerializeField] AudioSource suckNoise; //0
    [SerializeField] AudioSource popNoise; //1
    [SerializeField] AudioSource boingNoise; //2
    [SerializeField] AudioSource InteractNoise; //4
    [SerializeField] AudioSource LockedDoor; //5
    [SerializeField] AudioSource PartyHorn; //6
    [SerializeField] AudioSource AngelicChords; 
    [SerializeField] AudioSource BeeJees;
    [SerializeField] AudioSource BlackEyedPeas;
    [SerializeField] AudioSource PauseSound; 

    [Header("UI")]
    [SerializeField] GameObject endScreen; 
    [SerializeField] GameObject endVideo; 
    //[SerializeField] Text keyCount; 
    [SerializeField] Text keyCardCount; 
    [SerializeField] GameObject pauseMenu; 
    [SerializeField] GameObject overlayScreen; 
    [SerializeField] Text savedBees;  
    [SerializeField] Text beeCounter;  
    [SerializeField] Text totalScore; 
    [SerializeField] GameObject needAKey; 
    [SerializeField] GameObject pressE; 
    [SerializeField] GameObject rightClick; 
    [SerializeField] GameObject pressEToRelease; 
    [SerializeField] GameObject needTheQueen; 
    int keys = 0;
    int keyamount =0; 
    int beeCount = 0; 
    int score = 0; 

    [Header("Gun")]
    [SerializeField] Animator gunSuck; 
    [SerializeField] GameObject gun; 

    [Header("HideAndSeek")]
    [SerializeField] ParticleSystem EndParticles; 
    [SerializeField] GameObject NemoObj; 
    [SerializeField] GameObject CakeObj; 
    [SerializeField] GameObject Key1; 
    [SerializeField] GameObject Key2; 
    [SerializeField] GameObject Bee1; 
    [SerializeField] GameObject Key3; 
    [SerializeField] GameObject Key4; 
    [SerializeField] GameObject Key5; 
    [SerializeField] GameObject Bee2; 
    [SerializeField] GameObject Bee3; 
    [SerializeField] GameObject Light1; 
    [SerializeField] GameObject Light2;
    [SerializeField] GameObject Light3;
    [SerializeField] GameObject Light4;
    [SerializeField] GameObject Light5;
    [SerializeField] GameObject Light6;
    [SerializeField] GameObject Light7;
    [SerializeField] GameObject Light8;
    [SerializeField] GameObject Light9;
    [SerializeField] GameObject Light10;  
    bool hasSearched1 = false; 
    bool hasSearched2 = false; 
    bool hasSearched3 = false; 
    bool hasSearched4 = false; 
    bool hasSearched5 = false; 
    bool hasSearched6 = false; 
    bool hasSearched7 = false; 
    bool hasSearched8 = false; 
    [SerializeField] Animator BoxStack; //Search 5
    [SerializeField] Animator CarOne;   //Search 2
    [SerializeField] Animator CarTwo;   //Search 1
    [SerializeField] Animator Dumpster; //Search 3
    [SerializeField] Animator CafeteriaChair;   //Search 4
    [SerializeField] Animator Couch;    //Search 6
    [SerializeField] Animator Poster;   //Search 7
    [SerializeField] Animator OfficeChair;  //Search 8
    [SerializeField] Animator VendingMachine;   //CakeBees 
    [SerializeField] Animator Bin;  //FindNemo 

    float time; 
    bool paused; 
 
    void Start()
    {
        Time.timeScale = 1f; 

        controller = GetComponent<CharacterController>();

        inputManager = InputManager.Instance; 
        cameraTransform = Camera.main.transform; 

        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false; 

        keyCardCount.text = "0"; 
        beeCounter.text = "0"; 
        Achievements = 0; 

        Confetti.Stop(); 
        Confetti.Clear();
        EndParticles.Stop(); 
        EndParticles.Clear(); 

        gunSuck = gun.GetComponent<Animator>(); 
        savedTheBees = false; 
        haveFirstStep = false; 
        FoundNemo = false; 
        haveCake = false; 
    }

    void Update() //player jumping
    {
        groundedPlayer = controller.isGrounded;

        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = 0f;
        }

        Vector2 movement = inputManager.GetPlayerMovement(); 
        Vector3 move = new Vector3(movement.x, 0f, movement.y);
        move = cameraTransform.forward * move.z + cameraTransform.right *move.x; 
        move.y = 0f; 
        controller.Move(move * Time.deltaTime * playerSpeed);
        
        if (inputManager.PlayerJumped() && groundedPlayer)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            //AudioSource.PlayClipAtPoint(boingNoise, Sounds.transform.position); 
            boingNoise.Play (); 
        }

        playerVelocity.y += gravityValue * Time.deltaTime;
        controller.Move(playerVelocity * Time.deltaTime);

    }

    void FixedUpdate()
    {
        Vector3 rayOrigin = new Vector3 (0.5f, 0.5f, 0f); 
        RaycastHit hit; 
        Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
        //Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

        if ((Physics.Raycast(ray, out hit, rayLength, leMask)) || (Physics.Raycast(ray, out hit, rayLength, queenMask)))
        {
            StartCoroutine(RightClicker()); 
        } 

        if (Physics.Raycast(ray, out hit, rayLength, keyMask) || Physics.Raycast(ray, out hit, rayLength, doormask) || Physics.Raycast(ray, out hit, rayLength, Search1) || Physics.Raycast(ray, out hit, rayLength, Search2))
        {
            StartCoroutine(PressE()); 
        }

        if (Physics.Raycast(ray, out hit, rayLength, EndGameMask))
        {
            StartCoroutine(ReleaseBees()); 
        }
    }

    void Awake() 
    {
        playerController = new PlayerController(); 
        playerController.Enable(); 

        playerController.Actions.Pause.performed += PauseGame; 
        playerController.Actions.Shoot.performed += ShootHoney; 
        playerController.Actions.Interact.performed += Interaction; 

        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false; 
    }

    void OnDestroy()
    {
        playerController.Actions.Pause.performed -= PauseGame; 
        playerController.Actions.Shoot.performed -= ShootHoney; 
        playerController.Actions.Interact.performed -= Interaction; 
    }

    void PauseGame (InputAction.CallbackContext ctx)
    {
        paused = !paused; 
        if (paused)
        {
            PauseSound.Play(); 
            Cursor.lockState = CursorLockMode.None; 
            Cursor.visible = true; 
            pauseMenu.SetActive(true);
            overlayScreen.SetActive(false); 
            Time.timeScale = 0f; 
            playerController.Disable(); 
        } else 
        {
            PauseSound.Play(); 
            Cursor.lockState = CursorLockMode.Locked; 
            Cursor.visible = false; 
            pauseMenu.SetActive(false);
            overlayScreen.SetActive(true); 
            Time.timeScale = 1f; 
            playerController.Enable(); 
        }
    }

    void ShootHoney(InputAction.CallbackContext Shoot)
    {
       if (Shoot.performed)
        {
            suckNoise.Play(); 
            StartCoroutine(GunAnimation()); 
            //AudioSource.PlayClipAtPoint(suckNoise, playerCamera.transform.position); 
            Vector3 rayOrigin = new Vector3 (0.5f, 0.5f, 0f); 
            RaycastHit hit; 
            Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
            //Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

            if (Physics.Raycast(ray, out hit, rayLength, leMask))
            {
                if (hit.collider.gameObject.layer == 10)
                {
                    if (firstBees)
                    {
                        Destroy(hit.transform.gameObject); 
                        //AudioSource.PlayClipAtPoint(popNoise, playerCamera.transform.position); 
                        popNoise.Play(); 
                        beeCount += 1; 
                        beeCounter.text = beeCount.ToString();
                        score += 100; 

                        StartCoroutine(FirstBeePopUp()); 
                    }
                    else 
                    {
                        Destroy(hit.transform.gameObject); 
                        //AudioSource.PlayClipAtPoint(popNoise, playerCamera.transform.position); 
                        popNoise.Play(); 
                        beeCount += 1; 
                        beeCounter.text = beeCount.ToString();
                        score += 100; 
                    }
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, queenMask))
            {   
                if (hit.collider.gameObject.layer == 13)
                {
                    Destroy(hit.transform.gameObject); 
                    beeCount += 1;
                    beeCounter.text = beeCount.ToString(); 
                    score += 200;  
                    haveQueen = true; 
                    StartCoroutine(LordOfTheCrowns());  
                }
            } 

        }
    }

    void Interaction(InputAction.CallbackContext Interact)
    {
        if (Interact.performed)
        {
            Vector3 rayOrigin = new Vector3 (0.5f, 0.5f, 0f); 
            RaycastHit hit; 
            Ray ray = Camera.main.ViewportPointToRay(rayOrigin);
            //Debug.DrawRay(ray.origin, ray.direction * rayLength, Color.red);

            if (Physics.Raycast(ray, out hit, rayLength, keyMask))
            {
                if ((hit.collider.gameObject.layer == 11)) //key
                {
                    if (!havefirstkey)
                    {
                        //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                        InteractNoise.Play(); 
                        Destroy(hit.transform.gameObject); 
                        keys += 1; 
                        keyamount += 1; 
                        keyCardCount.text = keyamount.ToString();
                        score += 50; 

                        StartCoroutine(FirstKeyPopUp()); 
                    }
                    else
                    {
                        //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                        InteractNoise.Play(); 
                        Destroy(hit.transform.gameObject); 
                        keys += 1; 
                        keyamount += 1; 
                        keyCardCount.text = keyamount.ToString();
                        score += 50; 
                    }
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, doormask)) //doors
            {
                if ((hit.collider.gameObject.layer == 12) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 12) && (keyamount >= 1))
                {
                    if (!firstDoor)
                    {
                        //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                        InteractNoise.Play(); 
                        Destroy(hit.transform.gameObject); 
                        score += 50; 
                        keyamount -= 1; 
                        keyCardCount.text = keyamount.ToString();

                        StartCoroutine(FirstDoorPopUp());
                    }
                    else
                    {
                        //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                        InteractNoise.Play(); 
                        Destroy(hit.transform.gameObject); 
                        score += 50; 
                        keyamount -= 1; 
                        keyCardCount.text = keyamount.ToString();
                    }
                }

            }

            if (Physics.Raycast(ray, out hit, rayLength, ThirdFloor))
            {
                if ((hit.collider.gameObject.layer == 22) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 22)&& (keyamount >= 1))
                {
                    //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                    InteractNoise.Play(); 
                    Destroy(hit.transform.gameObject); 
                    score += 50; 
                    keyamount -= 1; 
                    keyCardCount.text = keyamount.ToString();

                    StartCoroutine(LunchRoomOpen()); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, FourthFloor))
            {
                if ((hit.collider.gameObject.layer == 23) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 23)&& (keyamount >= 1))
                {
                    //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                    InteractNoise.Play(); 
                    Destroy(hit.transform.gameObject); 
                    score += 50; 
                    keyamount -= 1; 
                    keyCardCount.text = keyamount.ToString();

                    StartCoroutine(Loveisanopendoor()); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, SecondFloor))
            {
                if ((hit.collider.gameObject.layer == 21) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 21) && (keyamount >= 1))
                {
                    //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                    InteractNoise.Play(); 
                    Destroy(hit.transform.gameObject); 
                    score += 100; 
                    keyamount -= 1; 
                    keyCardCount.text = keyamount.ToString();

                    StartCoroutine(EntireOffice());
                }

            }

            if (Physics.Raycast(ray, out hit, rayLength, FifthFloor))
            {
                if ((hit.collider.gameObject.layer == 24) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 24) && (keyamount >= 1))
                {
                    //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                    InteractNoise.Play(); 
                    Destroy(hit.transform.gameObject); 
                    score += 100; 
                    keyamount -= 1; 
                    keyCardCount.text = keyamount.ToString();

                    StartCoroutine(MeetMeHalfwayRight());
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Roofmask))
            {
                if ((hit.collider.gameObject.layer == 25) && (keyamount <= 0))
                {
                    //AudioSource.PlayClipAtPoint(LockedDoor, playerCamera.transform.position);
                    LockedDoor.Play(); 
                    StartCoroutine(NeedAKeyPopUp()); 
                }

                if ((hit.collider.gameObject.layer == 25) && (keyamount >= 1))
                {
                    //AudioSource.PlayClipAtPoint(InteractNoise, playerCamera.transform.position); 
                    InteractNoise.Play(); 
                    Destroy(hit.transform.gameObject); 
                    score += 100; 
                    keyamount -= 1; 
                    keyCardCount.text = keyamount.ToString();

                    StartCoroutine(Doaflip());
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, FindNemoKey))
            {
                if ((hit.collider.gameObject.layer == 9))
                {
                    InteractNoise.Play(); 
                    InteractNoise.Play(); 
                    StartCoroutine(FindNemo()); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, CakeBees))
            {   
                if (hit.collider.gameObject.layer == 26)
                {
                    InteractNoise.Play(); 
                    StartCoroutine(CakeLiesToYou()); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search1))
            {
                if (hit.collider.gameObject.layer == 8)
                {
                    InteractNoise.Play(); 
                    SearchOne(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search2))
            {
                if (hit.collider.gameObject.layer == 15)
                {
                    InteractNoise.Play(); 
                    SearchTwo(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search3))
            {
                if (hit.collider.gameObject.layer == 18)
                {
                    InteractNoise.Play(); 
                    SearchThree(); 
                }
            }

            if(Physics.Raycast(ray, out hit, rayLength, Search4))
            {
                if (hit.collider.gameObject.layer == 19)
                {
                    InteractNoise.Play(); 
                    SearchFour(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search5))
            {
                if (hit.collider.gameObject.layer == 20)
                {
                    InteractNoise.Play(); 
                    SearchFive(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search6))
            {
                if (hit.collider.gameObject.layer == 29)
                {
                    InteractNoise.Play(); 
                    SearchSix(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search7))
            {
                if (hit.collider.gameObject.layer == 30)
                {
                    InteractNoise.Play(); 
                    SearchSeven(); 
                }
            }

            if (Physics.Raycast(ray, out hit, rayLength, Search8))
            {
                if (hit.collider.gameObject.layer == 17)
                {
                    InteractNoise.Play(); 
                    SearchEight(); 
                }
            }
            
            if (Physics.Raycast(ray, out hit, rayLength, EndGameMask))
            {   
                if (hit.collider.gameObject.layer == 27 && haveQueen) 
                { 
                    StartCoroutine(SaveTheBeesAch()); 
                }
                else 
                {
                    StartCoroutine(FindQueen()); 
                }
            }  
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 28)
        {
            //bool
            StartCoroutine(SmallStepsAch()); 
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.layer == 28)
        {
            StopCoroutine(SmallStepsAch()); 
        }
    }

    /* ACHIEVEMENTS */

    IEnumerator SaveTheBeesAch ()
    {
        if (!savedTheBees)
        {
            Achievements += 1; 
            savedTheBees = true; 
            PartyHorn.Play(); 
            Confetti.Play(); 
            A_11.SetActive(true); 
            yield return new WaitForSeconds(2f); 
            A_11.SetActive(false); 
            Confetti.Stop(); 
            Confetti.Clear(); 
            StartCoroutine(GameEnd()); 
        }
        StopCoroutine(SaveTheBeesAch()); 
    }

    IEnumerator FirstBeePopUp ()
    {
        Achievements += 1; 
        PartyHorn.Play(); 
        Confetti.Play(); 
        A_1.SetActive(true);
        Catchem.GetComponent<Text>().color = Color.black; 
        Tick1.SetActive(true); 
        firstBees = false; 
        yield return new WaitForSeconds(2f); 
        A_1.SetActive(false); 
        Confetti.Stop(); 
        Confetti.Clear(); 
        StopCoroutine(FirstBeePopUp()); 
    }

    IEnumerator FirstKeyPopUp ()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_2.SetActive(true); 
        Confetti.Play(); 
        GainAccess.GetComponent<Text>().color = Color.black; 
        havefirstkey = true; 
        Tick2.SetActive(true); 
        yield return new WaitForSeconds (2f); 
        A_2.SetActive(false); 
        Confetti.Stop(); 
        Confetti.Clear();
        StopCoroutine(FirstKeyPopUp()); 
    }

    IEnumerator FirstDoorPopUp ()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_12.SetActive(true); 
        Confetti.Play(); 
        OpenADoor.GetComponent<Text>().color = Color.black; 
        Tick12.SetActive(true); 
        firstDoor = true; 
        yield return new WaitForSeconds(2f); 
        A_12.SetActive(false); 
        Confetti.Stop(); 
        Confetti.Clear();
        StopCoroutine(FirstDoorPopUp()); 
    }

    IEnumerator EntireOffice()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_5.SetActive(true); 
        Confetti.Play(); 
        ExcelAtExploring.GetComponent<Text>().color = Color.black;
        Tick5.SetActive(true); 
        yield return new WaitForSeconds (2f); 
        A_5.SetActive(false); 
        Confetti.Stop(); 
        Confetti.Clear();
        StopCoroutine(EntireOffice()); 
    }

    IEnumerator SmallStepsAch ()
    {
        if (!haveFirstStep)
        {
            haveFirstStep = true; 
            stepsTrigger.SetActive(false);
            Achievements += 1;
            PartyHorn.Play(); 
            Confetti.Play(); 
            A_4.SetActive(true); 
            OneSmallStep.GetComponent<Text>().color = Color.black;
            Tick4.SetActive(true); 
            yield return new WaitForSeconds(2f); 
            Confetti.Stop(); 
            Confetti.Clear();
            A_4.SetActive(false); 
        }
            StopCoroutine(SmallStepsAch()); 
    }

    IEnumerator LunchRoomOpen()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_6.SetActive(true); 
        Confetti.Play();
        LunchTime.GetComponent<Text>().color = Color.black;
        Tick6.SetActive(true); 
        yield return new WaitForSeconds(2f);
        Confetti.Stop(); 
        Confetti.Clear();
        A_6.SetActive(false); 
    }

    IEnumerator Loveisanopendoor()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_7.SetActive(true); 
        Confetti.Play(); 
        Tick7.SetActive(true); 
        LoveIsAnOpenDoor.GetComponent<Text>().color = Color.black;
        yield return new WaitForSeconds(2f); 
        Confetti.Stop(); 
        Confetti.Clear();
        A_7.SetActive(false); 
    }

    IEnumerator MeetMeHalfwayRight ()
    {
        BeeJees.Stop();
        BlackEyedPeas.Play();
        Achievements += 1;
        PartyHorn.Play(); 
        A_8.SetActive(true);
        Confetti.Play(); 
        Tick8.SetActive(true); 
        MeetMeHalfway.GetComponent<Text>().color = Color.black;
        yield return new WaitForSeconds(2f); 
        Confetti.Stop(); 
        Confetti.Clear();
        A_8.SetActive(false); 
    }

    IEnumerator Doaflip ()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_9.SetActive(true); 
        Confetti.Play(); 
        Tick9.SetActive(true); 
        DoAFlip.GetComponent<Text>().color = Color.black;
        yield return new WaitForSeconds(2f); 
        Confetti.Stop(); 
        Confetti.Clear();
        EndParticles.Play(); 
        A_9.SetActive(false); 
    }

    IEnumerator LordOfTheCrowns ()
    {
        Achievements += 1;
        PartyHorn.Play(); 
        A_10.SetActive(true); 
        Confetti.Play();
        Tick10.SetActive(true); 
        OneCrown.GetComponent<Text>().color = Color.black;
        yield return new WaitForSeconds(2f); 
        Confetti.Stop(); 
        Confetti.Clear();
        A_10.SetActive(false); 
    }

    IEnumerator CakeLiesToYou ()
    {
        if (!haveCake)
        {
            VendingMachine.SetTrigger("Searched"); 
            Light1.SetActive(false);
            haveCake = true; 
            Achievements += 1;
            PartyHorn.Play(); 
            Confetti.Play(); 
            CakeObj.SetActive(true); 
            A_15.SetActive(true); 
            Tick14.SetActive(true); 
            TheCakeIsALie.GetComponent<Text>().color = Color.black;
            yield return new WaitForSeconds(2f);
            Confetti.Stop(); 
            Confetti.Clear();
            A_15.SetActive(false);  
        }
    }

    IEnumerator FindNemo ()
    {
        if (!FoundNemo)
        {
            Bin.SetTrigger("Searched"); 
            Light2.SetActive(false); 
            FoundNemo = true; 
            Achievements += 1;
            PartyHorn.Play(); 
            NemoObj.SetActive(true); 
            A_13.SetActive(true); 
            Confetti.Play(); 
            Tick13.SetActive(true); 
            FindingNemo.GetComponent<Text>().color = Color.black;
            yield return new WaitForSeconds(2f); 
            Confetti.Stop(); 
            Confetti.Clear();
            A_13.SetActive(false); 
        }
    }

    void SearchOne ()
    {
        if (!hasSearched1)
        {
            CarTwo.SetTrigger("Searched");
            hasSearched1 = true; 
            Light8.SetActive(false); 
            AngelicChords.Play();
            Key1.SetActive(true); 
        }
    }

    void SearchTwo ()
    {
        if (!hasSearched2)
        {
            CarOne.SetTrigger("Searched"); 
            hasSearched2 = true; 
            Light9.SetActive(false); 
            AngelicChords.Play();
            Key2.SetActive(true); 
        }
    }

    void SearchThree ()
    {
        if (!hasSearched3)
        {
            Dumpster.SetTrigger("Searched"); 
            hasSearched3 = true; 
            Light7.SetActive(false); 
            AngelicChords.Play();
            Bee1.SetActive(true); 
        }
    }

    void SearchFour ()
    {
        if (!hasSearched4)
        {
            CafeteriaChair.SetTrigger("Searched");
            hasSearched4 = true; 
            Light6.SetActive(false); 
            AngelicChords.Play();
            Key3.SetActive(true); 
        }
    }

    void SearchFive ()
    {
        if (!hasSearched5)
        {
            BoxStack.SetTrigger("Searched"); 
            hasSearched5 = true; 
            Light10.SetActive(false); 
            AngelicChords.Play();
            Key4.SetActive(true); 
        }
    }

    void SearchSix ()
    {
        if (!hasSearched6)
        {
            Couch.SetTrigger("Searched"); 
            hasSearched6 = true; 
            Light5.SetActive(false); 
            AngelicChords.Play();
            Key5.SetActive(true); 
        }
    }

    void SearchSeven ()
    {
        if (!hasSearched7)
        {
            Poster.SetTrigger("Searched"); 
            hasSearched7 = true; 
            Light4.SetActive(false);
            AngelicChords.Play();
            Bee2.SetActive(true); 
        }
    }

    void SearchEight ()
    {
        if (!hasSearched8)
        {
            OfficeChair.SetTrigger("Searched"); 
            hasSearched8 = true; 
            Light3.SetActive(false); 
            AngelicChords.Play();
            Bee3.SetActive(true); 
        }
    }

    /* POPUPS */
    IEnumerator NeedAKeyPopUp()
    {
        needAKey.SetActive(true); 
        yield return new WaitForSeconds(2f); 
        needAKey.SetActive(false); 
        StopCoroutine(NeedAKeyPopUp()); 
    }

    IEnumerator FindQueen()
    {
        needTheQueen.SetActive(true); 
        yield return new WaitForSeconds(2f); 
        needTheQueen.SetActive(false); 
        StopCoroutine(FindQueen()); 
    }

    IEnumerator RightClicker ()
    {
        time = 2; 

        while (time > 0)
        {
            time -= 1 * Time.deltaTime; 
            rightClick.SetActive(true);  
            yield return null; 
        }

        rightClick.SetActive(false); 
        StopCoroutine(RightClicker()); 
    }

    IEnumerator GunAnimation ()
    {
        gunSuck.SetTrigger("SuckPerformed"); 
        yield return new WaitForSeconds (0.5f); 
        gunSuck.ResetTrigger("SuckPerformed"); 
        StopCoroutine(GunAnimation()); 
    }

    IEnumerator PressE ()
    {
        time = 2; 

        while (time > 0)
        {
            time -= 1 * Time.deltaTime; 
            pressE.SetActive(true); 
            yield return null;
        }
        
        pressE.SetActive(false); 
        StopCoroutine(PressE()); 
    }

    IEnumerator ReleaseBees ()
    {
        time = 2; 

        while (time > 0)
        {
            time -= 1 * Time.deltaTime; 
            pressEToRelease.SetActive(true); 
            yield return null; 
        }

        pressEToRelease.SetActive(false); 
        StopCoroutine(ReleaseBees()); 
    }

    IEnumerator GameEnd ()
    {
        time = 3; 
        EndParticles.Stop(); 
        EndParticles.Clear(); 

        while (time > 0)
        {
            time -= 1 * Time.deltaTime; 
            endVideo.SetActive(true); 
            yield return null; 
        }
        
        endVideo.SetActive(false); 
        overlayScreen.SetActive(false); 
        endScreen.SetActive(true); 
        Time.timeScale = 0f; 
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true; 
        Text(); 
    }

    void Text ()
    {
        savedBees.text = beeCount.ToString(); 
        totalScore.text = score.ToString();
        achievementNo.text = Achievements.ToString(); 
    }
}