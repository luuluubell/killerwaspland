﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations; 

public class AnimationController : MonoBehaviour
{
    [SerializeField] Animator BoxStack; 
    [SerializeField] Animator CarOne;
    [SerializeField] Animator CarTwo;
    [SerializeField] Animator Dumpster;
    [SerializeField] Animator CafeteriaChair;
    [SerializeField] Animator Couch;
    [SerializeField] Animator Poster;
    [SerializeField] Animator OfficeChair;
    [SerializeField] Animator VendingMachine;
    [SerializeField] Animator Bin;   

    public void SearchBox ()
    {
        BoxStack.SetTrigger("Searched"); 
    }

    public static void SearchCarOne ()
    {

    }


}
