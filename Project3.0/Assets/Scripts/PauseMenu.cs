﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] GameObject pause;
    [SerializeField] GameObject playerCamera;
    [SerializeField] GameObject overlayScreen; 
    [SerializeField] GameObject ControlScreen; 
 
    public void Resume ()
    {
        Time.timeScale = 1f; 
        pause.SetActive(false); 
        overlayScreen.SetActive(true); 
        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false; 
        PlayerMovement.playerController.Enable(); 
    }
    
    public void MainMenu ()
    {
        SceneManager.LoadScene ("MainMenu"); 
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true; 
    }

    public void QuitGame()
    {
        Application.Quit(); 
    }

    public void ReplayGame ()
    {
        SceneManager.LoadScene("GameScene"); 
    }

    public void Controls ()
    {
        pause.SetActive(false); 
        ControlScreen.SetActive(true); 
    }

    public void BackToPause ()
    {
        ControlScreen.SetActive(false); 
        pause.SetActive(true); 
    }

}
