﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warnings : MonoBehaviour
{
    [SerializeField] GameObject MainMenu;
    [SerializeField] GameObject Warning1; 
    [SerializeField] GameObject Warning2; 
    [SerializeField] GameObject WarningScreens; 
    [SerializeField] GameObject NextButton1; 
    [SerializeField] GameObject NextButton2; 
    public void NextWarning()
    {
        Warning1.SetActive(false); 
        Warning2.SetActive(true); 
        NextButton1.SetActive (false); 
        NextButton2.SetActive(true); 
    }

    public void MainScreen()
    {
        MainMenu.SetActive(true); 
        WarningScreens.SetActive(false); 
    }
}
