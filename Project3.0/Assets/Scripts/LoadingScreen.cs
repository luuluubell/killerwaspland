﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LoadingScreen : MonoBehaviour
{
    [SerializeField] GameObject loadingScreen; 
    [SerializeField] GameObject overlayScreen;  
    [SerializeField] Rigidbody player; 
    void Awake()
    {
        player.isKinematic = true; 
        loadingScreen.SetActive(true); 
        StartCoroutine("Load"); 
        overlayScreen.SetActive(false); 
        Time.timeScale = 0f; 
    }

    IEnumerator Load ()
    {
        yield return new WaitForSeconds(5f); 
        loadingScreen.SetActive(false); 
        overlayScreen.SetActive(true); 
        Time.timeScale = 1f;

        player.isKinematic = false; 
        StopCoroutine("Load"); 
    }
}
