﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement; 

public class MainMenu : MonoBehaviour
{
    [SerializeField] GameObject canvas1; 
    [SerializeField] GameObject canvas2; 
    private void Awake()
    {
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true; 
    }

    public void PlayGame ()
    {
        SceneManager.LoadScene("GameScene"); 
    }

    public void QuitGame ()
    {
        Application.Quit(); 
    }

    public void Credits ()
    {
        canvas1.SetActive(false); 
        canvas2.SetActive(true); 
    }

    public void Back ()
    {
        canvas2.SetActive(false); 
        canvas1.SetActive(true); 
    }

}
